package ru.sav;

public class Request {
    private String name;
    private Integer amount;
    private RequestType type;

    Request(String name, Integer amount, RequestType type) {
        this.name = name;
        this.amount = amount;
        this.type=type;
    }

    public String getName() {
        return name;
    }

    public Integer getAmount() {
        return amount;
    }

    public RequestType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Request{" +
                "name='" + name + '\'' +
                ", amount=" + amount +
                ", type=" + type +
                '}';
    }
}
