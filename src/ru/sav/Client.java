package ru.sav;

public class Client implements Runnable {
    private String name;
    private Frontend front;

    private RequestType requestType;
    private Integer amount;

    public Client(String name, Frontend front, RequestType requestType, Integer amount) {
        this.name = name;
        this.requestType = requestType;
        this.front = front;
        this.amount = amount;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            Request req = new Request(this.name, amount, this.requestType);
            System.out.println(this.name + ": ������ " + req + " �������");
            front.setRequest(req);
        }
    }
}
