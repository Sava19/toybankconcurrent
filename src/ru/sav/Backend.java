package ru.sav;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Supplier;

public class Backend {
    /*
    *����� ������������ Atomic ��� �������
    *����������� ����, ����� ����������� ����� � �������
     */
    private Integer balance;
    private ReentrantLock lock = new ReentrantLock();

    Backend(Integer balance) {
        this.balance = balance;
    }

    private void printBalance() {
        System.out.println("������: " + this.balance);
    }

    public void increaseBalance(String processorName, String clientName, Integer amount) {
        try {
            lock.lock();
            try {
                balance += amount;
                System.out.println(processorName + ": ������ �� " + clientName + " ����������. ���������� �� " + amount + " ���.");
                printBalance();
            } catch (Exception e) {
                System.out.println(processorName + ": ������ �� " + clientName + " �� ����������. ");
                System.out.println(e.toString());
                printBalance();
            }
        } finally {
            lock.unlock();
        }
    }

    public void decreaseBalance(String processorName, String clientName, Integer amount) {
        try {
            lock.lock();
            if ((balance - amount) < 0) {
                System.out.println(processorName + ": ������ �� " + clientName + " �� ������ " + amount + " �� ����������. ������������ �������");
            } else {
                balance -= amount;
                System.out.println(processorName + ": ������ �� " + clientName + " ����������. ������ " + amount + " ���.");
            }
            printBalance();
        } finally {
            lock.unlock();
        }


    }


}
