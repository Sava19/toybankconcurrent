package ru.sav;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void init(int initialBalance) {
        Backend backend = new Backend(initialBalance);
        Frontend frontend = new Frontend();

        ExecutorService serviceProcessors = Executors.newFixedThreadPool(2);
        serviceProcessors.submit(new Processor("��������� 1", backend, frontend));
        serviceProcessors.submit(new Processor("��������� 2", backend, frontend));

        ExecutorService serviceClients = Executors.newFixedThreadPool(4);
        serviceClients.submit(new Client("������ 1", frontend, RequestType.TOPUP, 100));
        serviceClients.submit(new Client("������ 2", frontend, RequestType.TOPUP, 50));
        serviceClients.submit(new Client("������ 3", frontend, RequestType.WITHDRAW, 800));
        serviceClients.submit(new Client("������ 4", frontend, RequestType.WITHDRAW, 200));

        serviceClients.shutdown();
        try {
            serviceClients.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            System.out.println("Interrupted");
        }

        serviceProcessors.shutdownNow();
    }

    public static void main(String[] args) {

        List<CompletableFuture<Integer>> systemsFuture = BankSystem.createBankSystems(3);
        CompletableFuture<Void> allFutures = CompletableFuture.allOf(systemsFuture.toArray(new CompletableFuture[0]));

        allFutures.thenAccept(v -> {
            int newBalance = 0;
            for (CompletableFuture<Integer> systemFuture : systemsFuture) {
                newBalance +=systemFuture.join();
            }
            init(newBalance);
        });

        allFutures.join();

    }
}
