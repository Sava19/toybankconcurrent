package ru.sav;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletableFuture;

public class BankSystem {
    private static int MIN_DELAY = 5000;
    private static int MAX_DELAY = 10000;
    private static int MAX_AMOUNT = 1000;

    private static CompletableFuture<Integer> calculateBalance()  {
        return CompletableFuture.supplyAsync(() -> {
            Random rand = new Random();
            int randomInt = MIN_DELAY + rand.nextInt((MAX_DELAY - MIN_DELAY) + 1);
            try {
                Thread.sleep(randomInt);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int amount = rand.nextInt(MAX_AMOUNT);
            System.out.println("���������� �������: ����� " + amount);
            return amount;
        });

    }

    public static List<CompletableFuture<Integer>> createBankSystems(int count) {
        List<CompletableFuture<Integer>> systemsFuture = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            systemsFuture.add(calculateBalance());
        }
        return systemsFuture;
    }
}
