package ru.sav;

public class Processor implements Runnable {
    private String name;
    private Backend backend;
    private Frontend frontend;

    Processor(String name, Backend backend, Frontend frontend) {
        this.name = name;
        this.backend = backend;
        this.frontend = frontend;
    }

    private void checkRequests() {
        Request req = frontend.getRequest();
        if (req != null) {
            if (req.getType() == RequestType.TOPUP) {
                backend.increaseBalance(this.name, req.getName(), req.getAmount());
            } else {
                backend.decreaseBalance(this.name, req.getName(), req.getAmount());
            }
        }
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            checkRequests();
        }
        Thread.currentThread().interrupt();
    }
}
