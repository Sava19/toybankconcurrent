package ru.sav;

import java.util.concurrent.*;

public class Frontend {
    private static final int MAX_REQUEST_COUNT = 2;

    /*
     *���������� ����������� ������ �������, ��� ������������� ������������� lock � Condition
     *�������, ���������� �� LinkedList, ��������� � ����� � ��������� �� ������ �� ������� �������
     */
    private BlockingQueue<Request> queue = new LinkedBlockingQueue<>(MAX_REQUEST_COUNT);


    public void setRequest(Request req) {
        try {
            queue.put(req);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Request getRequest() {
        try {
            return queue.take();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return null;
        }
    }

}
